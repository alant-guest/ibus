Source: ibus
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders: Aron Xu <aron@debian.org>, Osamu Aoki <osamu@debian.org>
Build-Depends: dbus (>= 1.8),
               debhelper-compat (= 12),
               desktop-file-utils,
               dh-python,
               gettext,
               gobject-introspection,
               gnome-common,
               gtk-doc-tools,
               intltool (>= 0.40.0),
               iso-codes,
               libjson-glib-dev,
               libdbus-glib-1-dev,
               libdconf-dev,
               libgirepository1.0-dev,
               libglib2.0-dev (>= 2.0.0),
               libgtk-3-dev (>= 3.7.0),
               libgtk2.0-dev (>= 2.24.5-4),
               libnotify-dev (>= 0.7),
               libtool,
               libwayland-dev [linux-any],
               pkg-config (>= 0.16),
               python-gi-dev,
               python3-all,
               python3-dbus,
               qtbase5-dev,
               unicode-cldr-core,
               unicode-data,
               valac (>= 0.16)
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/debian/ibus
Vcs-Git: https://salsa.debian.org/debian/ibus.git
Homepage: https://github.com/ibus/ibus

Package: ibus
Architecture: any
Multi-Arch: foreign
Depends: dconf-cli,
         gir1.2-gtk-3.0 (>=3.8.5),
         gir1.2-ibus-1.0 (= ${binary:Version}),
         adwaita-icon-theme,
         librsvg2-common,
         python3-gi,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: ibus-clutter,
            ibus-gtk,
            ibus-gtk3,
            ibus-qt4,
            libqt5gui5,
            im-config
Suggests: ibus-doc
Breaks: ibus-anthy (<< 1.5.3-2),
        ibus-el (<< 0.3.2-2),
        ibus-googlepinyin (<< 0.1.2-2)
Description: Intelligent Input Bus - core
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.

Package: libibus-1.0-5
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Intelligent Input Bus - shared library
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains shared library

Package: libibus-1.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-ibus-1.0 (= ${binary:Version}),
         libdbus-1-dev,
         libglib2.0-dev,
         libibus-1.0-5 (= ${binary:Version}),
         ${misc:Depends}
Replaces: libibus-dev
Description: Intelligent Input Bus - development file
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains the header files and static libraries which are
 needed for developing the IBus applications.

Package: ibus-gtk
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Intelligent Input Bus - GTK+2 support
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains the GTK+2 IM module.

Package: ibus-gtk3
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Intelligent Input Bus - GTK+3 support
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains the GTK+3 IM module.

Package: ibus-wayland
Architecture: linux-any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Intelligent Input Bus - Wayland support
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains the Wayland IM module.

Package: ibus-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Intelligent Input Bus - development documentation
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains the reference manual.

Package: gir1.2-ibus-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends}, ${misc:Depends}
Description: Intelligent Input Bus - introspection data
 IBus is an Intelligent Input Bus. It is a new input framework for the Linux
 OS. It provides full featured and user friendly input method user interface.
 It also may help developers to develop input method easily.
 .
 This package contains the GObject introspection data which are needed
 for developing the IBus applications in various programming languages
 with GObject introspection support.

